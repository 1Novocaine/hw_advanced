<?php

namespace App\Models;


class User extends Model
{


    public static $props = [
        'id' => 'id',
        'email' => 'email',
        'login' => 'login',
        'password' => 'password',
    ];
}